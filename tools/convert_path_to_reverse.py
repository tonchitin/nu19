import argparse
import numpy as np
import csv
import io

class Entry(object):
    def __init__(self, x, y, velocity, acceleration, jerk, heading, dt, position):
        self.x = x
        self.y = y
        self.velocity = velocity
        self.acceleration = acceleration
        self.jerk = jerk
        self.heading = heading
        self.dt = dt
        self.position = position
        
    def __repr__(self):
        return "x={} y={}".format(self.x, self.y)
        
    def copy(self):
        return Entry(**self.__dict__)

    
def l2_distance(pt1, pt2):
    return np.sqrt((pt1.x - pt2.x)**2 + (pt1.y - pt2.y)**2)
def norm_angle(a):
    while a <= -180.:
        a += 360.
    while a > 180.:
        a -= 360.
    return a
class Path(object):
    FIELDS = ["dt", "x", "y", "position", "velocity", "acceleration", "jerk", "heading"]

    @classmethod
    def from_file(cls, fd, name=None):
        reader = csv.DictReader(fd)
        entries = []
        for row in reader:
            flts = {k:float(v) for k,v in row.items()}
            entries.append(Entry(**flts))
        return Path(entries, name=name)
    def __init__(self, entries, reverse=False, name=None):
        self.entries = entries
        self.reverse = reverse
        self.name = name or "UnNamed"
    def serialize(self, fd):
        writer = csv.DictWriter(fd, self.FIELDS)
        writer.writeheader()
        writer.writerows([e.__dict__ for e in self.entries])
    def tank_modify(self, wheel_base_width):
        """Split path into left/right.

        Cribbed from (https://github.com/JacisNonsense/Pathfinder/blob/master/Pathfinder-Core/src/modifiers/tank.c)

        Returns:
            (Path, Path): Left/right tuple.
        """
        left_entries = []
        right_entries = []
        w = wheel_base_width / 2.

        def _interpolate_velocity(entry, last, left):
            dist = l2_distance(entry, last)
            if self.reverse:
                dist *= -1
            left.position = last.position + dist
            left.velocity = dist / entry.dt
            left.acceleration = (left.velocity - last.velocity) / entry.dt
            left.jerk = (left.acceleration - last.acceleration) / entry.dt

        start_head = self.entries[0].heading
        for i, entry in enumerate(self.entries):
            entry.heading -= start_head
            
            left = entry.copy()
            right = entry.copy()
            left.x = entry.x - w * np.sin(entry.heading)
            left.y = entry.y + w * np.cos(entry.heading)
            if i > 0:
                last = left_entries[i-1]
                _interpolate_velocity(left, last, left)

            right.x = entry.x + w * np.sin(entry.heading)
            right.y = entry.y - w * np.cos(entry.heading)

            if i > 0:
                last = right_entries[i - 1]
                _interpolate_velocity(right, last, right)

            left_entries.append(left)
            right_entries.append(right)
        left_str = "Left" if self.reverse else "Right"
        right_str = "Right" if self.reverse else "Left"
        left_path = Path(left_entries, reverse=self.reverse, name=self.name+"-"+left_str)
        right_path = Path(right_entries, reverse=self.reverse, name=self.name+"-"+right_str)
        if self.reverse:
            return left_path, right_path
        return left_path, right_path

    def rewrite_reverse(self):
        """Rewrite the path as if it is going backwards.
        """
        rev_entries = []
        n = len(self.entries)

        for i, entry in enumerate(self.entries):
            seg = entry.copy()
            seg.position *= -1
            seg.velocity *= -1
            seg.acceleration *= -1
            seg.jerk *= -1
            seg.x = self.entries[n-i-1].x
            seg.y = self.entries[n-i-1].y
            seg.heading = np.deg2rad(norm_angle(np.rad2deg(entry.heading * -1)))
            
            rev_entries.append(seg)
        rev_name = self.name + "-Reversed"
        return Path(rev_entries, reverse=True, name=rev_name)

def parse_args():
    parser = argparse.ArgumentParser(description="Convert path from forward to reverse.")
    parser.add_argument("filename", help="Path to file CSV.")
    return parser.parse_args()

def main():
    args = parse_args()
    name = args.filename.split(".")[0]
    path = Path.from_file(open(name + ".pf1.csv"), name=name)
    rev_path = path.rewrite_reverse()
    left_path, right_path = rev_path.tank_modify(1.823)
    with open(name + "Reverse.left.pf1.csv", "w") as fd:
        left_path.serialize(fd)
    with open(name + "Reverse.right.pf1.csv", "w") as fd:
        right_path.serialize(fd)

if __name__ == "__main__":
    main()
