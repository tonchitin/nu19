package frc.team125.robot;

public class RobotMap {

  public static final int WRIST_MOTOR = 10;
  public static final int PIVOT_MOTOR = 14;
  public static final int ARM_MOTOR = 12;
  public static final int CARGO_INTAKE_MOTOR = 15;

  //Drivetrain
  public static final int LEFT_DRIVE_MAIN = 1;
  public static final int RIGHT_DRIVE_MAIN = 6;

  public static final int LEFT_DRIVE_FOLLOWER_A = 2;
  public static final int RIGHT_DRIVE_FOLLOWER_A = 5;

  //Suction Cup
  public static final int SUCTION_MOTOR = 9;

  // Solenoid

  public static final String LIMELIGHT_FRONT = "limelight-fcam";
  public static final String LIMELIGHT_BACK = "limelight-bcam";
  public static final int CLAMP_RELEASE_SOLENOID = 2;
  public static final int CLAMP_PIVOT_SOLENOID = 1;
  public static final int CARGO_SOLENOID = 0;

  // Vision
  public static final int TARGET_HEIGHT = 37;
  public static final int CAM_HEIGHT = 43;
  public static final int CAMERA_ANGLE = 0;
  public static final double ROCKET_PORT_HEIGHT = 39.125;

}
