/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.team125.robot.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.WaitCommand;

/**
 * Command group that goes from the player station and back twice.
 *
 * @author rotciV Lumbago
 * @see h Kelly if this doesn't work because it's her fault
 */
public class ToPlayerSAndBack extends CommandGroup {
  double timeout = 1.5; //Seconds

  /**
   * Why are all of these constructors checkstyle issues.
   */

  public ToPlayerSAndBack() {
    //Go from start to the rocket
    addSequential(new DrivePathCommand("StartToRocketShort"));
    addSequential(new AlignToTargetAndMoveForward());
    addSequential(new WaitCommand(timeout));

    //Reverse to player station
    addSequential(new DrivePathCommand("RocketToPlayerStationReverse"));
    addSequential(new WaitCommand(timeout));

    //Go forward to the rocket hatch port
    addSequential(new DrivePathCommand("PlayerStationToRocketShort"));
    addSequential(new AlignToTargetAndMoveForward());
    addSequential(new WaitCommand(timeout));

    //Reverse back to the player station
    addSequential(new DrivePathCommand("RocketToPlayerStationReverse"));
    addSequential(new WaitCommand(timeout));

    //Go forward to the hatch port
    addSequential(new DrivePathCommand("PlayerStationToRocketShort"));
    addSequential(new AlignToTargetAndMoveForward());

  }
}
