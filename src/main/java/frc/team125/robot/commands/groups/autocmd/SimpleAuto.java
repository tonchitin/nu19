package frc.team125.robot.commands.groups.autocmd;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.team125.robot.commands.DrivePid;

public class SimpleAuto extends CommandGroup {
  Command driveSimple = new DrivePid();

  public SimpleAuto() {
    addSequential(driveSimple);
  }
}

