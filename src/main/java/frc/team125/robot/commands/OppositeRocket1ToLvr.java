/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.team125.robot.commands;

import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.WaitCommand;

public class OppositeRocket1ToLvr extends CommandGroup {
  /**
   * Why is this a checkstyle issue.
  */

  public OppositeRocket1ToLvr() {
    addSequential(new DrivePathCommand("StartToRocketShort").withTargetDistance(50.0));
    addSequential(new AlignToTargetAndMoveForward());
    addSequential(new WaitCommand(1.5));

    addSequential(new DrivePathCommand("RocketToPlayerStationReverse"));
    addSequential(new WaitCommand(1.5));

    addSequential(new DrivePathCommand("PlayerStationToRocketShort"));
    addSequential(new AlignToTargetAndMoveForward());

  }
}
