/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.team125.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.Robot;
import frc.team125.robot.RobotMap;
import frc.team125.robot.subsystems.Drivetrain;

/**
 * Command that Aligns the robot with the hatch port.
 *
 * @author rotciV Lumbago
 * @see h Kelly if this doesn't work because it's her fault
 */
public class AlignToTargetAndMoveForward extends Command {
  private static double angleThresh = 0.7;
  private static double desiredTargetDistance = 50;
  private static double driveKp = Drivetrain.DrivetrainConstants.drive_kP;
  private static double autoKp = Drivetrain.DrivetrainConstants.auto_kP;
  private static double skewKp = 0.02;
  private static boolean useBackCam = false;
  private static double lastError = 0;

  public AlignToTargetAndMoveForward() {
    requires(Robot.dt);
    requires(Robot.vision);
  }

  public AlignToTargetAndMoveForward(boolean useBack) {
    this.useBackCam = useBack;
  }
  // public AlignAndPlace(double desiredDistance) {
  //   this.desiredTargetDistance = desiredDistance;
  // }
  // public AlignAndPlace(double desiredDistance, boolean useBack){
  //   this.desiredTargetDistance = desiredDistance;
  //   this.useBackCam = useBack;
  // }

  @Override
  protected void initialize() {
    Robot.dt.enableBrakeMode();
    // if (useBackCam){
    //   Robot.frontVision.setRearCamFeed();
    // }
  }

  @Override
  protected void execute() {
    double kp = 0.009;
    double ki = 0;
    double kd = 0;
    double accumulatedError = 0;
    SmartDashboard.putNumber("Auto Turn", Drivetrain.DrivetrainConstants.auto_kP * Robot.vision.getXOffset());
    double targetSkew = Robot.vision.getTargetSkew();
    double distanceToTarget = Robot.vision.calculateDistanceFromCameraHeight(
        RobotMap.TARGET_HEIGHT, RobotMap.CAM_HEIGHT, RobotMap.CAMERA_ANGLE);
    double distanceToTargetDesiredDifference = distanceToTarget - desiredTargetDistance;

    // if (useBackCam){
    //   Robot.dt.driveAssisted(-0.03, Robot.frontVision.getXOffset() * -autoKp);
    // }
    // else {
    double errorDiff = distanceToTargetDesiredDifference - lastError;
    accumulatedError += distanceToTargetDesiredDifference;

    Robot.dt.driveAssisted(
        distanceToTargetDesiredDifference * kp + kd * errorDiff + ki * accumulatedError, 
        Robot.vision.getXOffset() * autoKp);
    lastError = distanceToTargetDesiredDifference;

    // }
  }

  @Override
  protected boolean isFinished() {
    return Math.abs(Robot.vision.getXOffset()) < angleThresh
        && Robot.vision.calculateDistanceFromCameraHeight(
          RobotMap.TARGET_HEIGHT, RobotMap.CAM_HEIGHT, RobotMap.CAMERA_ANGLE) < desiredTargetDistance + 5
        && Robot.vision.calculateDistanceFromCameraHeight(
          RobotMap.TARGET_HEIGHT, RobotMap.CAM_HEIGHT, RobotMap.CAMERA_ANGLE) > desiredTargetDistance - 5;
  }

  @Override
  protected void end() {
    Robot.dt.drive(0, 0);
  }

  @Deprecated
  public void deprecated() {

  }

  @Override
  protected void interrupted() {
  }
}
