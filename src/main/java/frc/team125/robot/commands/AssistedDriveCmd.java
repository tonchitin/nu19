package frc.team125.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.Robot;
import frc.team125.robot.subsystems.Drivetrain;

public class AssistedDriveCmd extends Command {

  // Threshold of how far off the target we can be for the drive assisted to work
  private static double thresh = 0.7;
  private static double max_offset = 20;

  public AssistedDriveCmd() {
    requires(Robot.dt);
    requires(Robot.vision);
  }

  protected void initialize() {
    Robot.dt.enableBrakeMode();
  }

  // Called repeatedly when this Command is scheduled to run
  protected void execute() {
    SmartDashboard.putNumber("Auto Turn", Drivetrain.DrivetrainConstants.auto_kP * Robot.vision.getXOffset());
    if (Math.abs(Robot.vision.getXOffset()) > thresh
        && Math.abs(Robot.vision.getXOffset()) < max_offset) {
      Robot.dt.driveAssisted(
          Robot.oi.getDriverTriggerSum(), Robot.vision.getXOffset() * Drivetrain.DrivetrainConstants.auto_kP);
    } else if (Math.abs(Robot.vision.getXOffset()) > max_offset) {
      Robot.dt.driveArcade(Robot.oi.getDriverTriggerSum(), Robot.oi.getDriverLeftStickX());
    } else {
      Robot.dt.driveArcade(Robot.oi.getDriverTriggerSum(), 0.0);
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  protected void interrupted() {
  }
}

