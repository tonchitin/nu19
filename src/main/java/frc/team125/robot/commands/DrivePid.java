package frc.team125.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import frc.team125.robot.Robot;

public class DrivePid extends Command {

  private static double goal = 50;

  public DrivePid() {
    requires(Robot.dt);
  }

  protected void initialize() {
    Robot.dt.setPidConstants();

  }

  protected void execute() {
    Robot.dt.runPosPid(goal, goal);
  }

  protected void end() {
    Robot.dt.drive(0.0, 0.0);
  }

  protected void interrupted() {
    Robot.dt.drive(0.0, 0.0);
  }

  @Override
  protected boolean isFinished() {
    return Robot.dt.isPidComplete();
  }
}
