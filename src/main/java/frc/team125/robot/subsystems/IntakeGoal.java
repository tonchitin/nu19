package frc.team125.robot.subsystems;

public class IntakeGoal {
  public enum CargoIntakeGoal {
    NONE, RUN_FORWARD, RUN_REVERSE;
  }

  public enum WristIntakeState {
    IDLE(0.0), ACQUIRING(1.0), CARRYING(0.3), SCORING(0.0);

    private double pow;

    WristIntakeState(double pow) {
      this.pow = pow;
    }

    public double getPow() {
      return this.pow;
    }
  }
}
