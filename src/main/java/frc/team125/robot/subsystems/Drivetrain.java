package frc.team125.robot.subsystems;

import com.kauailabs.navx.frc.AHRS;
import com.revrobotics.CANPIDController;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import com.revrobotics.ControlType;
import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.Robot;
import frc.team125.robot.RobotMap;
import frc.team125.robot.commands.ArcadeDriveCmd;
import frc.team125.robot.utils.CsvWriter;

import java.util.Date;
import java.util.HashMap;


public class Drivetrain extends Subsystem {
  private static final double HIGH_POW = 1.0;
  private static final double LOW_POW = -HIGH_POW;
  private static final double RAMP_RATE = 0.25;
  public CANSparkMax leftDriveMain = new CANSparkMax(RobotMap.LEFT_DRIVE_MAIN,
      MotorType.kBrushless);
  public double leftGoal = 0.0;
  public double rightGoal = 0.0;
  Date dtDate;
  AHRS gyro = new AHRS(I2C.Port.kMXP);
  CANPIDController autoLController;
  CANPIDController autoRController;
  // Initialize the last head error to be zero
  double lastHeadingError = 0.0;
  int drivetrainDate = 0;
  /* private TalonSRX leftDriveMain = new TalonSRX(RobotMap.LEFT_DRIVE_MAIN);
   private TalonSRX leftDriveFollower = new TalonSRX(RobotMap.LEFT_DRIVE_FOLLOWER);
   private TalonSRX rightDriveMain = new TalonSRX(RobotMap.RIGHT_DRIVE_MAIN);
   private VictorSPX rightDriveFollower = new VictorSPX(RobotMap.RIGHT_DRIVE_FOLLOWER);
 */
  private double lastRightEncoder;
  private double lastLeftEncoder;
  private CANSparkMax leftDriveFollowerA = new CANSparkMax(RobotMap.LEFT_DRIVE_FOLLOWER_A,
      MotorType.kBrushless);
  private CANSparkMax rightDriveMain = new CANSparkMax(RobotMap.RIGHT_DRIVE_MAIN,
      MotorType.kBrushless);
  private CANSparkMax rightDriveFollowerA = new CANSparkMax(RobotMap.RIGHT_DRIVE_FOLLOWER_A,
      MotorType.kBrushless);
  private boolean assisted;
  private CsvWriter csvWriter = new CsvWriter("/home/lvuser/logs/drivetrain", new String[] {"velocity"});

  /**
   * Class for the robot drivetrain. Running 6 NEO motors per side, with SPARK Max Controllers
   */
  public Drivetrain() {
    /*
    this.leftDriveFollowerB.follow(leftDriveMain);

    this.rightDriveFollowerB.follow(rightDriveMain);

    this.rightDriveFollowerB.setInverted(true);
    this.leftDriveFollowerB.setInverted(false);*/
    this.rightDriveMain.setInverted(true);
    this.rightDriveFollowerA.follow(rightDriveMain);
    this.leftDriveMain.setInverted(false);
    this.leftDriveFollowerA.follow(leftDriveMain);
    this.assisted = false;

    this.autoLController = this.leftDriveMain.getPIDController();
    this.autoRController = this.rightDriveMain.getPIDController();

    this.autoLController.setOutputRange(DrivetrainConstants.kMinOutput,
        DrivetrainConstants.kMaxOutput);
    this.autoRController.setOutputRange(DrivetrainConstants.kMinOutput,
        DrivetrainConstants.kMaxOutput);

    leftDriveMain.enableVoltageCompensation(12.0);
    rightDriveMain.enableVoltageCompensation(12.0);
  }

  /**
   * Sets the SPARK Max Controllers to be in BrakeMode. Needed for driving in autonomous.
   * Very hard to move robot when in BrakeMode, even when disabled.
   */
  public void enableBrakeMode() {
    this.rightDriveMain.setIdleMode(IdleMode.kBrake);
    this.rightDriveFollowerA.setIdleMode(IdleMode.kBrake);
    this.leftDriveMain.setIdleMode(IdleMode.kBrake);
    this.leftDriveFollowerA.setIdleMode(IdleMode.kBrake);
  }

  /**
   * Sets the SPARK Mac Controllers to be in CoastMode. Needed for driving in teleoperated.
   * Much easier to move the robot when in CoastMode.
   */
  public void enableCoastMode() {
    this.rightDriveMain.setIdleMode(IdleMode.kCoast);
    this.rightDriveFollowerA.setIdleMode(IdleMode.kCoast);
    this.leftDriveMain.setIdleMode(IdleMode.kCoast);
    this.leftDriveFollowerA.setIdleMode(IdleMode.kCoast);
  }

  public void drive(double powLeft, double powRight) {
    leftDriveMain.set(powLeft);
    rightDriveMain.set(powRight);
  }

  public void driveArcade(double throttle, double turn) {
    drive(throttle + turn * 0.75, throttle - turn * 0.75);
  }

  // TODO Account for latency in the camera somehow
  public void driveAssisted(double throttle, double angleOffset) {
    double correction = angleOffset * DrivetrainConstants.kP - angleOffset * DrivetrainConstants.kD;
    drive(throttle + correction, throttle - correction);
  }

  public boolean getAssisted() {
    return this.assisted;
  }

  // Gyro methods

  public void setAssisted(boolean mode) {
    this.assisted = mode;
  }

  public double getGyroRate() {
    return this.gyro.getRate(); // TODO Check the units on this somehow
  }

  public double getAngle() {
    return this.gyro.getAngle();
  }

  public void resetGyro() {
    this.gyro.reset();
  }

  public double getGyroAngle() {
    return -this.gyro.getAngle();
  }

  /**
   * Sets all the PID Constants, so the SPARk Max Controllers know what to use in auto.
   */
  public void setPidConstants() {
    this.autoLController.setP(DrivetrainConstants.auto_kP);
    this.autoLController.setI(DrivetrainConstants.auto_kI);
    this.autoLController.setD(DrivetrainConstants.auto_kD);
    this.autoLController.setIZone(DrivetrainConstants.auto_kIz);
    this.autoLController.setFF(DrivetrainConstants.auto_kFF);
    this.autoRController.setP(DrivetrainConstants.auto_kP);
    this.autoRController.setI(DrivetrainConstants.auto_kI);
    this.autoRController.setD(DrivetrainConstants.auto_kD);
    this.autoRController.setIZone(DrivetrainConstants.auto_kIz);
    this.autoRController.setFF(DrivetrainConstants.auto_kFF);
  }

  /**
   * Runs the Position PID.
   *
   * @param goalLeftPos  The goal position of the left hand side for the PID Controller.
   * @param goalRightPos The goal position of the right hand side for the PID Controller.
   */
  public void runPosPid(double goalLeftPos, double goalRightPos) {
    this.leftGoal = goalLeftPos;
    this.rightGoal = goalRightPos;
    this.autoLController.setReference(goalLeftPos, ControlType.kPosition);
    this.autoRController.setReference(goalRightPos, ControlType.kPosition);
  }

  /**
   * Runs the Velocity PID.
   *
   * @param goalLeftVel  The goal velocity of the left hand side for the PID Controller.
   * @param goalRightVel The goal velocity of the right hand side for the PID Controller.
   */
  public void runVelPid(double goalLeftVel, double goalRightVel) {
    this.leftGoal = goalLeftVel;
    this.rightGoal = goalRightVel;
    this.autoLController.setReference(goalLeftVel, ControlType.kPosition);
    this.autoRController.setReference(goalRightVel, ControlType.kPosition);
  }

  public double getLeftVel() {
    return leftDriveMain.getEncoder().getVelocity();
  }

  /**
   * Updates the SmartDashboard with all the relevant drivetrain values.
   */
  public void updateSmartDashboard() {
    dtDate = new Date();
    SmartDashboard.putNumber("Right Enc Pos:", this.getRightEncoderPosition());
    SmartDashboard.putNumber("Left Enc Pos:", this.getLeftEncoderPosition());
    SmartDashboard.putNumber("Right Enc Vel:", this.leftDriveMain.getEncoder().getVelocity());
    SmartDashboard.putNumber("Left Enc Vel:", this.leftDriveMain.getEncoder().getVelocity());
    double feetPerRev = DrivetrainConstants.kWheelDiameter * Math.PI;
    double ticksPerRev = 9.0;
    double ticksPerSec = this.leftDriveMain.getEncoder().getVelocity() / 60.0;
    HashMap<String, Object> entries = new HashMap<>();
    entries.put("velocity", ticksPerSec / ticksPerRev * feetPerRev);
    this.csvWriter.update(entries);
    writeLogs();
    SmartDashboard.putNumber("Angle", getAngle());

    SmartDashboard.putNumber("Left Wheel Pos:", this.getLeftEncoderPosition() / ticksPerRev * feetPerRev);
    SmartDashboard.putNumber("Left Wheel Velocity", ticksPerSec / ticksPerRev * feetPerRev);
    // Ticks/sec / (Ticks/rotation) (feet/rotation)
    SmartDashboard.putNumber("Right Output:", this.rightDriveMain.get());
    SmartDashboard.putNumber("Left Output:", this.leftDriveMain.get());
  }

  public void writeLogs() {
    csvWriter.write();
  }

  public boolean isPidComplete() {
    return Math.abs(this.getLeftEncoderPosition() - this.leftGoal) < 10
        && Math.abs(this.getRightEncoderPosition() - this.rightGoal) < 10;
  }

  public double getRightEncoderPosition() {
    return -this.rightDriveMain.getEncoder().getPosition();
  }


  public double getLeftEncoderPosition() {
    return this.leftDriveMain.getEncoder().getPosition();
  }

  // Methods for driving autonomously and other autonomous function

  // Read in a trajectory from a file


  @Override
  protected void initDefaultCommand() {
    setDefaultCommand(new ArcadeDriveCmd());
  }

  // Static class for handling the drivetrain PID stuff
  public static class DrivetrainConstants {
    // Vision PID constants
    public static double kP = 0.04;
    public static double kD = 0.00;

    // Autonomous PID constants
    public static double auto_kP = 0.75;
    public static double auto_kI = 1e-4;
    public static double auto_kD = 1;
    public static double auto_kIz = 0;
    public static double auto_kFF = 0;
    public static double drive_kP = 0.01;
    public static double kMaxOutput = 1;
    public static double kMinOutput = -1;

    public static double kWidth = (21.875) / 12.0;

    public static double kWheelDiameter = 5.0 / 12.0; // inches;
    public static double kTicksPerRev = 4096; // For the CTRE mag encoder

    public static double kdt = 0.02;
    public static double kMaxVelocity = 12.5;
    public static double kMaxAcceleration = 27.0;

  }
}
