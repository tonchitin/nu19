/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.team125.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.TalonSRXConfiguration;

import edu.wpi.first.wpilibj.command.Subsystem;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.RobotMap;
import frc.team125.robot.utils.CsvWriter;

import java.util.HashMap;

public class Arm extends Subsystem {
  //  private static String timestamp = new SimpleDateFormat("yyyyMMddHHmmss")
  //         .format(new java.util.Date()) + "arm";
  private static String[] loggedFields = {"armQuadPos", "armPWMPos", "armVoltage", "armCurrent"};
  private CsvWriter armLogger;

  private TalonSRX armMotor;
  private boolean calibrated = false;
  private double currGoal = 0.0;
  private ArmState currState = ArmState.IDLE;
  private double tolerance = 200; // TODO tune this
  private double outputPower = 0.0;

  public enum ArmState {
    DISABLED, IDLE, MAGIC, MANUAL, HOLDING, POSITION
  }

  /**
   * Subsystem representing the telescoping arm part of the superstructure.
   */
  public Arm() {
    // this.armLogger = new CsvWriter("/home/lvuser/logging/" + timestamp, loggedFields);

    this.armMotor = new TalonSRX(RobotMap.ARM_MOTOR);

    TalonSRXConfiguration config = new TalonSRXConfiguration();
    config.peakOutputForward = Constants.Arm.MAX_POWER;
    config.peakOutputReverse = -Constants.Arm.MAX_POWER;
    config.nominalOutputForward = 0.0;
    config.nominalOutputReverse = 0.0;

    armMotor.configAllSettings(config);

    this.armMotor.setSensorPhase(true);

    this.armMotor.getSensorCollection().setQuadraturePosition(
            this.armMotor.getSensorCollection().getPulseWidthPosition(), 0);

    this.armMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 0);

    configMotionMagic(Constants.Arm.CRUISE_VELOCITY_UP, Constants.Arm.CRUISE_ACCEL_UP);

    this.armMotor.setNeutralMode(NeutralMode.Brake);
  }

  /**
   * The main update method. This is called every time we want the robot to be updated. Should be
   * called every update cycle of the robot.
   */
  public void update() {

    switch (this.currState) {
      case DISABLED:
        this.outputPower = 0.0;
        this.runArm();
        break;
      case IDLE:
        this.outputPower = 0.0;
        this.runArm();
        break;
      case MAGIC:
        this.runMotionMagic();
        if (this.checkTermination()) {
          this.setHoldingPid();
          this.currState = ArmState.HOLDING;
        }
        break;
      case POSITION:
        this.runPosition();
        if (this.checkTermination()) {
          this.setHoldingPid();
          this.currState = ArmState.HOLDING;
        }
        break;
      case MANUAL:
        runArm();
        break;
      case HOLDING:
        holdPosition();
        break;
      default:
        break;
    }

  }

  //////////////////////////////
  // ALL THE MOVEMENT METHODS //
  //////////////////////////////

  /**
   * Runs the arm to a goal using motion magic on the TalonSRXs.
   */
  public void runMotionMagic() {
    this.armMotor.set(ControlMode.MotionMagic, this.currGoal);
  }

  /**
   * Runs the arm to a specific position using PID on the TalonSRXs.
   */
  public void runPosition() {
    this.armMotor.set(ControlMode.Position, this.currGoal);
  }

  /**
   * Runs teh arm based on the output power set in setStaticOutput. Different from runArmManual.
   * This should be used in the state machine, the other should be used for testing only.
   */
  public void runArm() {
    armMotor.set(ControlMode.PercentOutput, this.outputPower);
  }

  /**
   * Manually runs arm.
   *
   * @param pow arm power
   */
  public void runArmManual(double pow) {
    this.outputPower = pow;
    this.armMotor.set(ControlMode.PercentOutput, this.outputPower);
  }

  /**
   * Holds the arm at a specific position using the Position Closed Loop Control.
   */
  public void holdPosition() {
    // Check to see if we're actually at position
    // If we're not at position, don't try to move, just hold the position
    this.setHoldingPid();
    this.runPosition();
  }


  /////////////////////////////////
  // ALL THE SETTERS AND CONFIGS //
  /////////////////////////////////

  /**
   * Configures the velocity and acceleration for motion magic.
   *
   * @param cruiseVelocity     The max velocity of the system
   * @param cruiseAcceleration The max acceleration of the system
   */
  public void configMotionMagic(int cruiseVelocity, int cruiseAcceleration) {
    armMotor.configMotionCruiseVelocity(cruiseVelocity);
    armMotor.configMotionAcceleration(cruiseAcceleration);
  }

  /**
   * Sets the goal, and the state of the arm to start the state machine.
   *
   * @param g The new goal position for the arm to go to
   */
  public void setMagicPosGoal(double g) {
    // TODO keep an eye on this fucking shit up later
    if (Math.abs(g - this.currGoal) <= this.tolerance * 2) {
      this.currState = ArmState.HOLDING;
      this.setHoldingPid();
    } else {
      if (g < this.currGoal) {
        // retracting
        this.setRetractingPid();
      } else if (g > this.currGoal) {
        // Extending
        this.setExtendingPid();
      }
      this.currState = ArmState.MAGIC;
    }

    this.currGoal = Math.max(0.0, g);
  }

  /**
   * Sets the state of the arm manually.
   *
   * @param s Desired arm state.
   */
  public void setState(ArmState s) {
    this.currState = s;
  }

  /**
   * This is used for setting a static output to give to the arm. Will run the arm at a constant
   * power in the runArm method. This method is the one that should be called in the state machien.
   */
  public void setStaticOutput(double pow) {
    this.outputPower = pow;
  }

  /**
   * Sets the new PID values for extending the arm.
   */
  public void setExtendingPid() {
    configMotionMagic(Constants.Arm.CRUISE_VELOCITY_UP, Constants.Arm.CRUISE_ACCEL_UP);
    // FULL OUTPUT IS 1023
    this.armMotor.config_kP(0, Constants.Arm.ARM_OUT_K_P, 0);
    this.armMotor.config_kI(0, Constants.Arm.ARM_OUT_K_I, 0);
    this.armMotor.config_kD(0, Constants.Arm.ARM_OUT_K_D, 0);
    this.armMotor.config_kF(0, 0.0, 0);
  }

  /**
   * Sets the new PID values for retracting the arm.
   */
  public void setRetractingPid() {
    configMotionMagic(Constants.Arm.CRUISE_VELOCITY_DOWN, Constants.Arm.CRUISE_ACCEL_DOWN);
    this.armMotor.config_kP(0, Constants.Arm.ARM_IN_K_P, 0);
    this.armMotor.config_kI(0, Constants.Arm.ARM_IN_K_I, 0);
    this.armMotor.config_kD(0, Constants.Arm.ARM_IN_K_D, 0);
    this.armMotor.config_kF(0, Constants.Arm.ARM_IN_K_F, 0);

  }

  /**
   * Sets the new PID values for holding the arm.
   */
  public void setHoldingPid() {
    configMotionMagic(Constants.Arm.CRUISE_VELOCITY_DOWN, Constants.Arm.CRUISE_ACCEL_DOWN);
    this.armMotor.config_kP(0, Constants.Arm.ARM_HOLDING_K_P, 0);
    this.armMotor.config_kI(0, Constants.Arm.ARM_HOLDING_K_I, 0);
    this.armMotor.config_kD(0, Constants.Arm.ARM_HOLDING_K_D, 0);
    this.armMotor.config_kF(0, Constants.Arm.ARM_HOLDING_K_F, 0);

  }


  /////////////////////
  // ALL THE GETTERS //
  /////////////////////

  /**
   * Simply returns the current number of encoder clicks.
   *
   * @return The current number of encoder clicks.
   */
  public int getEncoderClicks() {
    return this.armMotor.getSelectedSensorPosition(0);
  }

  /**
   * Calculates the length of arm extension in inches based on the current encoder position.
   *
   * @return The current length of extension.
   */
  public double getLengthInches() {
    return (Constants.ARM_ZERO - this.getEncoderClicks()) / Constants.CLICKS_PER_INCH;
  }

  /**
   * Calculates the number of encoders clicks for a given length of arm extension.
   *
   * @param length The length to convert, given in inches.
   * @return The equibalent number of encoder clicks.
   */
  public double inchesToClicks(double length) {
    return Constants.ARM_ZERO - (length * Constants.CLICKS_PER_INCH);
  }

  /**
   * Determines the arm's calibration state.
   *
   * @return The state of the arm being calibrated.
   */
  public boolean isCalibrated() {
    return this.calibrated;
  }

  /**
   * Calculates whether or not the arm is at the correct position and if it's reay to move to the
   * next part of the state machine.
   *
   * @return If arm is done moving/reached the termination point.
   */
  public boolean checkTermination() {
    return Math.abs(this.getEncoderClicks() - this.currGoal) <= this.tolerance;
  }

  /**
   * Considered done if the arm is in the HOLDING state.
   *
   * @return If the arm is done with it's set of motions.
   */
  public boolean isDone() {
    return this.currState == ArmState.HOLDING;
  }


  /**
   * Updates the SmartDashboard with all the relevant drivetrain values.
   */
  public void updateSmartDashboard() {
    SmartDashboard.putNumber("Arm Selected Sensor Position", this.getEncoderClicks());
    SmartDashboard.putNumber("Arm Quad Position",
            this.armMotor.getSensorCollection().getQuadraturePosition());
    SmartDashboard.putNumber("Arm Feedback Error:", this.armMotor.getClosedLoopError());
    SmartDashboard.putNumber("Arm Goal:", this.currGoal);
    SmartDashboard.putNumber("Arm Absolute",
            this.armMotor.getSensorCollection().getPulseWidthPosition());
    SmartDashboard.putString("Arm State:", this.currState.toString());
    SmartDashboard.putNumber("Arm Output:", this.armMotor.getMotorOutputVoltage());
    SmartDashboard.putString("Arm Mode:", this.armMotor.getControlMode().toString());
    SmartDashboard.putNumber("Arm Current:", this.armMotor.getOutputCurrent());
  }

  ///////////////////////////
  // ALL THE LOGGING STUFF //
  ///////////////////////////

  /**
   * ??StyleChecker sucks?? Updates arm logger.
   */
  public void updateLogger() {
    HashMap<String, Object> loggedValues = new HashMap<>();
    loggedValues.put("armQuadPos", armMotor.getSensorCollection().getQuadraturePosition());
    loggedValues.put("armPWMPos", armMotor.getSensorCollection().getPulseWidthPosition());
    loggedValues.put("armVoltage", outputPower);
    loggedValues.put("armCurrent", armMotor.getOutputCurrent());
    this.armLogger.update(loggedValues);
  }


  public void write() {
    this.armLogger.flushToFile();
  }

  @Override
  public void initDefaultCommand() {
  }

}
