package frc.team125.robot.subsystems;

import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Superstructure extends Subsystem {


  public enum SuperState {
    MOVING_PIVOT, RETRACTING, EXTENDING, HOLDING, IDLE, DISABLED
  }

  public enum IntakeSafety {
    RISING, FALLING, AT_POS, IDLE
  }

  private SuperState currState;
  private Arm arm;
  private Pivot pivot;
  private Wrist wrist;
  private CargoIntake cargoIntake;

  // Track the cargo intake state here
  private IntakeGoal.CargoIntakeGoal cargoGoal = IntakeGoal.CargoIntakeGoal.NONE;
  public IntakeSafety safety = IntakeSafety.IDLE;

  private SuperstructureGoal currGoal;
  private SuperstructureGoal lastGoal;
  public boolean acquiringHatch = true;


  /**
   * The main superstructure of the robot. Controls the motion of the pivot and the arm together.
   */
  public Superstructure() {
    this.arm = new Arm();
    this.pivot = new Pivot();
    this.wrist = new Wrist();
    this.cargoIntake = new CargoIntake();

    this.currState = SuperState.DISABLED;
    this.currGoal = new SuperstructureGoal(this.pivot.getEncoderClicks(),
            this.arm.getEncoderClicks(), this.wrist.getEncoderClicks());

    // Set the reflection state of the superstructure goals
    this.setReflections();
  }


  public boolean isCalibrated() {
    return this.arm.isCalibrated() && this.pivot.isCalibrated();
  }


  /**
   * The main update method. This calls the arm and pivot update methods as well. Handles all the
   * state machine logic of the robot's superstructure.
   */
  public void update() {

    // The Wrist will need to be handled separately
    switch (this.currState) {
      case DISABLED:
        this.pivot.setState(Pivot.ControlState.DISABLED);
        this.wrist.setState(Wrist.ControlState.DISABLED);
        this.arm.setState(Arm.ArmState.DISABLED);
        break;
      case MOVING_PIVOT:
        // The wrist will be holding when it's waiting to move (after intaking)
        // Or if it's finished it's motion
        if (this.pivot.isDone()) {
          this.currState = SuperState.EXTENDING;
          this.arm.setExtendingPid();
          this.arm.setMagicPosGoal(this.currGoal.armGoal);
          if (this.safety == IntakeSafety.RISING) {
            this.safety = IntakeSafety.IDLE;
            this.wrist.setMagicPosGoal(this.currGoal.wristGoal);
          }
        }
        break;
      case RETRACTING:
        // Check the wrist position
        // Pivot holding at "old" goal
        if (this.arm.isDone()) {
          this.currState = SuperState.MOVING_PIVOT;
          this.pivot.setMagicPosGoal(this.currGoal.pivotGoal);
        }
        break;
      case EXTENDING:
        // Pivot will hold at new goal
        if (this.arm.isDone() && this.wrist.isDone()) {
          this.currState = SuperState.HOLDING;
        }
        break;
      case HOLDING:
        if (this.safety == IntakeSafety.FALLING) {
          this.safety = IntakeSafety.AT_POS;
        }
        break;
      case IDLE:
        this.pivot.setState(Pivot.ControlState.IDLE);
        this.arm.setState(Arm.ArmState.IDLE);
        this.wrist.setState(Wrist.ControlState.IDLE);
        break;
      default:
        break;
    }


    switch (this.cargoGoal) {
      case NONE:
        this.cargoIntake.stopIntake();
        break;
      case RUN_FORWARD:
        this.cargoIntake.runIntake();
        break;
      case RUN_REVERSE:
        this.cargoIntake.reverseIntake();
        break;
      default:
        break;
    }

    // Wrist needs to run independently of the arm and pivot, but the arm should never extend
    // before the wrist has reached its position. Additionally, there will be some cases where the
    // wrist should move differently but we'll address those as they come up

    // If leaving the intake, we don't want to move the wrist quite yet. Instead we want to pivot
    // then move the wrist when "extending"


    this.pivot.update();
    this.arm.update();
    this.wrist.update(this.acquiringHatch);
  }

  /**
   * Sets all the goals of the superstructure, and sets the state machine into action.
   *
   * @param goal The new goal of the superstructure. Contains the individual arm and pivot goals.
   */
  public void setGoal(SuperstructureGoal goal) {
    if (this.currGoal.sameObject(goal)) {
      return;
    }
    this.lastGoal = this.currGoal;
    this.currGoal = goal;


    // If the last goal is reflectable and the new goal is also reflectable, then we can
    // move between the goals without having to retract the arm.
    // - Retract only if the arm goal of the new position is less than the old one. Then pivot.
    // - Pivot and then extend if the arm goal of the new position is greater than the old one.
    if (this.lastGoal.canBeReflected && this.currGoal.canBeReflected) {
      if (this.lastGoal.armGoal < this.currGoal.armGoal) {
        // Need to extend slightly
        this.currState = SuperState.MOVING_PIVOT;
        this.pivot.setMagicPosGoal(this.currGoal.pivotGoal); // Last pivot position
      } else if (this.lastGoal.armGoal > this.currGoal.armGoal) {
        // Need to retract slightly
        this.currState = SuperState.RETRACTING;
        this.arm.setMagicPosGoal(this.currGoal.armGoal);
      }
    } else {
      // If we're not reflecting between two goals that are reflectable, then we need to handle
      // the state machine transitions normally
      if (this.lastGoal.armGoal < Constants.ARM_ZERO) {
        this.currState = SuperState.MOVING_PIVOT;
        this.pivot.setMagicPosGoal(this.currGoal.pivotGoal); // Last pivot position
      } else {
        // Always want to go to safe zero first
        this.arm.setMagicPosGoal(Constants.ARM_ZERO); // Safe zero
        this.currState = SuperState.RETRACTING;
      }

      if (this.safety != IntakeSafety.RISING) {
        this.wrist.setMagicPosGoal(this.currGoal.wristGoal);
      } else {
        this.wrist.setMagicPosGoal(this.lastGoal.wristGoal);
      }
    }
  }

  /**
   * Sets the state of the wrist suction cup.
   *
   * @param state State of the wrist intake.
   */
  public void setWristIntakeState(IntakeGoal.WristIntakeState state) {
    this.wrist.setIntakeState(state);
  }


  /**
   * Prints the superstructure state to the smartdash. Also calls the arm and pivot smartdash
   * methods.
   */
  public void updateSmartDashboard() {
    this.arm.updateSmartDashboard();
    this.pivot.updateSmartDashboard();
    this.wrist.updateSmartDashboard();

    SmartDashboard.putString("Superstructure State:", this.currState.toString());
    SmartDashboard.putNumber("Sup Arm Goal:", this.currGoal.armGoal);
    SmartDashboard.putNumber("Sup Pivot Goal:", this.currGoal.pivotGoal);
    SmartDashboard.putNumber("Sup Wrist Goal:", this.currGoal.wristGoal);
    SmartDashboard.putString("Intake Safety:", this.safety.toString());
  }

  /**
   * This method initializes the superstructure and its state. Starts in idle mode. Can be used to
   * "reset" the superstructure so it stops trying to pursue any goals.
   */
  public void initialize() {
    this.currState = SuperState.IDLE;
    this.currGoal = new SuperstructureGoal(this.pivot.getEncoderClicks(),
            this.arm.getEncoderClicks(), this.wrist.getEncoderClicks());
  }

  /**
   * Set the reflected state of all the goals that can be reflected. If a goal is reflected, it
   * means it can move to any other reflected goal without having to retract the arm, just move
   * pivot to the correct angle and also retract/extend the arm if needed
   * Complementary scoring goals can be reflected: - Scoring high front / back with either cargo /
   * hatch - Scoring mid front / back with either cargo / hatch - Scoring low DOES NOT need to be
   * reflected
   */
  public void setReflections() {
    Constants.SCORE_CARGO_MID_FRONT.setReflection(true);
    Constants.SCORE_CARGO_HIGH_FRONT.setReflection(true);
    Constants.SCORE_CARGO_MID_BACK.setReflection(true);
    Constants.SCORE_CARGO_HIGH_BACK.setReflection(true);

    Constants.SCORE_HATCH_MID_FRONT.setReflection(true);
    Constants.SCORE_HATCH_HIGH_FRONT.setReflection(true);
    Constants.SCORE_HATCH_MID_BACK.setReflection(true);
    Constants.SCORE_HATCH_HIGH_BACK.setReflection(true);
  }

  @Override
  protected void initDefaultCommand() {

  }
}

