package frc.team125.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.TalonSRXConfiguration;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import frc.team125.robot.RobotMap;


public class CargoIntake extends Subsystem {
  private TalonSRX intakeMotor = new TalonSRX(RobotMap.CARGO_INTAKE_MOTOR);
  //private Solenoid intakePiston = new Solenoid(RobotMap.CARGO_SOLENOID);

  private static final double INTAKE_POW = 0.6;

  /**
   * The intake that picks the cargo up from the ground.
   */
  public CargoIntake() {
    TalonSRXConfiguration intakeConfig = new TalonSRXConfiguration();
    intakeConfig.peakOutputForward = Constants.Pivot.MAX_POWER;
    intakeConfig.peakOutputReverse = -Constants.Pivot.MAX_POWER;
    intakeConfig.nominalOutputForward = 0.0;

    intakeConfig.nominalOutputReverse = 0.0;


    intakeMotor.configAllSettings(intakeConfig);
  }

  public void runIntake() {
    intakeMotor.set(ControlMode.PercentOutput, INTAKE_POW);
    //intakePiston.set(true);
  }

  public void reverseIntake() {
    intakeMotor.set(ControlMode.PercentOutput, -INTAKE_POW);
    //intakePiston.set(true);
  }

  public void stopIntake() {
    intakeMotor.set(ControlMode.PercentOutput, 0);
    //intakePiston.set(false);
  }


  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
  }
}
