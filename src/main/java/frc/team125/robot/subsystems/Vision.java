package frc.team125.robot.subsystems;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.RobotMap;

public class Vision extends Subsystem {
  final double slope = -56.4;
  final double yintercept = 105.95;
  final double lightOn = 3.0;
  final double lightOff = 1.0;
  final double blink = 2.0;
  NetworkTable frontCamFeed;
  NetworkTable rearCamFeed;
  NetworkTable activeCamFeed;

  double tv;
  double tx;
  double ty;
  double ta;
  double ts;
  double tl;
  //Camera Dep
  double targetHeight;
  double cameraHeight;
  double cameraAngle;
  //Target Dependent
  double angleToTarget;
  double distanceToTarget;
  boolean porthacth = false;
  String camName;

  /**
   * Gets the X offset in degrees.
   *
   * @param targetHeight is used for the distance formulas.
   * @param cameraHeight is used for the distance formulas.
   * @param cameraAngle  is used for the distance formulas
   */
  public Vision(double targetHeight,
                double cameraHeight,
                double cameraAngle,
                String camName) {
    this.camName = camName;
    updateTableVariables();
    this.targetHeight = targetHeight;
    this.cameraHeight = cameraHeight;
    this.cameraAngle = cameraAngle;
  }

  /**
   * Gets the X offset in degrees.
   *
   * @param targetHeight is used for the distance formulas.
   * @param cameraHeight is used for the distance formulas.
   * @param cameraAngle  is used for the distance formulas
   */
  public Vision(double targetHeight, double cameraHeight, double cameraAngle) {
    frontCamFeed = NetworkTableInstance.getDefault().getTable(RobotMap.LIMELIGHT_FRONT);
    rearCamFeed = NetworkTableInstance.getDefault().getTable(RobotMap.LIMELIGHT_BACK);
    setRearCamFeed();
    updateTableVariables();
    this.targetHeight = targetHeight;
    this.cameraHeight = cameraHeight;
    this.cameraAngle = cameraAngle;
  }

  /**
   * Updates network frontCamFeed variables.
   */
  private void updateTableVariables() {
    tv = activeCamFeed.getEntry("tv").getDouble(0.0);
    tx = activeCamFeed.getEntry("tx").getDouble(0.0);
    ty = activeCamFeed.getEntry("ty").getDouble(0.0);
    ta = activeCamFeed.getEntry("ta").getDouble(0.0);
    ts = activeCamFeed.getEntry("ts").getDouble(0.0);
    tl = activeCamFeed.getEntry("tl").getDouble(0.0);
  }

  @Override
  public void initDefaultCommand() {
  }


  /**
   * Gets the X offset in degrees.
   *
   * @return The X offset in degrees.
   */
  public double getXOffset() {
    return tx;
  }

  /**
   * Gets the Y offset in radians.
   *
   * @return The Y offset in radians.
   */
  public double getYOffset() {
    return Math.toRadians(ty);
  }

  /**
   * Gets the target area.
   *
   * @return The target area.
   */
  public double getTargetArea() {
    return ta;
  }

  /**
   * Gets the target skew.
   *
   * @return Target Skew.
   */
  public double getTargetSkew() {
    return ts;
  }

  /**
   * Gets the X limelight's latency.
   *
   * @return Latency
   */
  public double getLatency() {
    return tl;
  }

  /**
   * Returns 1 if a target is visible, returns 0 if there's no target visible.
   *
   * @return 1 or 0
   */
  public double isTargetVisibleDouble() {
    return tv;
  }

  /**
   * Gets the distance of the target using the formula from the limelight's documentation.
   *
   * @return The distance to the target using a formula.
   */
  public double calculateDistanceFromCameraHeight(double targetHeight, double cameraHeight, double cameraAngle) {
    if (porthacth) {
      this.targetHeight = RobotMap.ROCKET_PORT_HEIGHT;
    }
    double methodDistance = (targetHeight - cameraHeight) / Math.tan(cameraAngle + getYOffset());
    return methodDistance;
  }

  /**
   * Gets the distance to the target based of off its area using a slope-intercept formula.
   *
   * @return The distance based off of the area of the target.
   */
  public double calculateDistanceArea() {
    return slope * ta + yintercept;
  }

  /**
   * Updates all of the target information available to SmartDashboard.
   */

  public void frontTurnOnLed() {
    frontCamFeed.getEntry("ledMode").setNumber(lightOn);
  }

  public void rearTurnOnLed() {
    rearCamFeed.getEntry("ledMode").setNumber(lightOn);
  }

  public void frontTurnOffLed() {
    rearCamFeed.getEntry("ledMode").setNumber(lightOff);
  }

  public void rearTurnOffLed() {
    rearCamFeed.getEntry("ledMode").setNumber(lightOff);
  }

  public void setFrontCamFeedA() {
    activeCamFeed = frontCamFeed;

  }

  public void setRearCamFeed() {
    activeCamFeed = rearCamFeed;
  }

  /**
   * Updates the values of the Vision class.
   */ 
  public void updateVisionDashboard() {
   /* updateTableVariables();
    SmartDashboard.putNumber("X Offset: ", getXOffset());
    SmartDashboard.putNumber("Y Offset: ", getYOffset());
    SmartDashboard.putNumber("Distance to target(Formula): ",
        calculateDistanceFromCameraHeight(this.targetHeight,
            this.cameraHeight,
            this.cameraAngle));
    SmartDashboard.putNumber("Distance to target(Area): ", calculateDistanceArea());
    SmartDashboard.putNumber("Target Area: ", getTargetArea());
    SmartDashboard.putNumber("Target Skew: ", getTargetSkew());
    SmartDashboard.putNumber("LimeLight Latency: ", getLatency());
    SmartDashboard.putNumber("Target Found (double): ", isTargetVisibleDouble());
*/
  }
}