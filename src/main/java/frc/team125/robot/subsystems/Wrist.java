/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.team125.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.TalonSRXConfiguration;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.team125.robot.RobotMap;
import frc.team125.robot.subsystems.IntakeGoal.WristIntakeState;

public class Wrist extends Subsystem {

  public enum ControlState {
    DISABLED, CALIBRATING, IDLE, MAGIC, MANUAL, HOLDING;
  }

  private TalonSRX wristMotor;
  private CANSparkMax suctionMotor;
  private Solenoid airRelease;
  private Solenoid pivotClamp;

  private IntakeGoal.WristIntakeState currIntakeState;
  private ControlState currState;

  // False is open (ie acquiring a hatch)
  private boolean clampState = false;
  // False is suction engaged
  private boolean suctionState = false;
  private boolean hasGamePiece = false;

  private double tolerance = 15; // TODO tune this
  private double currPosGoal = 0.0;
  private double currOutput = 0.0;

  /**
   * Subsystem representing the wrist part of the superstructure.
   */
  public Wrist() {
    this.wristMotor = new TalonSRX(RobotMap.WRIST_MOTOR);
    this.suctionMotor = new CANSparkMax(RobotMap.SUCTION_MOTOR, MotorType.kBrushless);


    TalonSRXConfiguration wristConfig = new TalonSRXConfiguration();
    wristConfig.peakOutputForward = Constants.Wrist.MAX_POWER;
    wristConfig.peakOutputReverse = -Constants.Wrist.MAX_POWER;
    wristConfig.nominalOutputForward = 0.0;
    wristConfig.nominalOutputReverse = 0.0;

    this.wristMotor.configAllSettings(wristConfig);

    this.wristMotor.setSensorPhase(false);
    this.wristMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, 0);
    this.wristMotor.getSensorCollection().setQuadraturePosition(
            this.wristMotor.getSensorCollection().getPulseWidthPosition(), 0);

    configMotionMagic(Constants.Wrist.CRUISE_VEL, Constants.Wrist.CRUISE_ACCEL);
    this.wristMotor.setNeutralMode(NeutralMode.Brake);

    // Configure the PID
    this.wristMotor.config_kP(0, Constants.Wrist.WRIST_K_P, 0);
    this.wristMotor.config_kI(0, Constants.Wrist.WRIST_K_I, 0);
    this.wristMotor.config_kD(0, Constants.Wrist.WRIST_K_D, 0);
    this.wristMotor.config_kF(0, Constants.Wrist.WRIST_K_F, 0);

    // INITIALIZE THE SOLENOIDS AND WRIST STATE
    this.airRelease = new Solenoid(RobotMap.CLAMP_RELEASE_SOLENOID);
    this.pivotClamp = new Solenoid(RobotMap.CLAMP_PIVOT_SOLENOID);

    this.currIntakeState = IntakeGoal.WristIntakeState.IDLE;
    this.currState = ControlState.IDLE;

    //this.openClamp();
  }


  /**
   * The main update function for the wrist where the state machine runs.
   *
   * @param hatch If we are we acquiring a hatch.
   */
  public void update(boolean hatch) {
    this.clampState = !hatch; // If "hatch" is false, then we want the hatch open (aka false)
    SmartDashboard.putString("intake State", this.currIntakeState.toString());
    // State of the suction cup, not necessarily the wrist motion
    switch (this.currIntakeState) {
      case IDLE:
        //this.suctionState = false; // Suction engaged
        break;
      case ACQUIRING:
        this.suctionState = false; // Suction engaged
        this.clampState = !hatch; // If "hatch" is false, then we want the hatch open (aka false)
        //this.engageSuction();
        // TODO Commenting out for now because this hasn't been tested
        /*this.hasGamePiece = this.getSuctionCurrent() > 20; // TODO obviously tune
        if (this.hasGamePiece) {
          this.currIntakeState = IntakeGoal.WristIntakeState.CARRYING;
        }*/
        break;
      case CARRYING:
        this.suctionState = false; // Suction engaged
        break;
      case SCORING:
        this.suctionState = true; // Suction disengaged
        break;
      default:
        break;

    }

    this.runSuction(this.currIntakeState.getPow());

    switch (this.currState) {
      case DISABLED:
        this.setStaticOutput(0.0);
        runWrist();
        this.currIntakeState = WristIntakeState.IDLE;
        break;
      case CALIBRATING:
        if (this.isCalibrated()) {
          this.currState = ControlState.IDLE;
        }
        break;
      case IDLE:
        this.setStaticOutput(0.0);
        runWrist();
        break;
      case MAGIC:
        runMotionMagic();
        if (this.checkTermination()) {
          this.currState = ControlState.HOLDING;
        }
        break;
      case MANUAL:
        //runWrist();
        break;
      case HOLDING:
        holdPosition();
        break;
      default:
        break;
    }


    this.pivotClamp.set(this.clampState);
    this.airRelease.set(this.suctionState);
  }

  //////////////////////////////
  // ALL THE MOVEMENT METHODS //
  //////////////////////////////

  /**
   * Runs the wrist based on the output power set in setStaticOutput. Different from runWristManual.
   * This should be used in the state machine, the other should be used for testing only.
   */
  public void runWrist() {
    wristMotor.set(ControlMode.PercentOutput, this.currOutput);
  }

  /**
   * Runs wrist manually. Use for testing only. Use runWrist and setStaticOutput for state machine.
   *
   * @param pow wrist power
   */
  public void runWristManual(double pow) {
    this.currOutput = pow;
    this.wristMotor.set(ControlMode.PercentOutput, this.currOutput);
  }

  /**
   * Runs wrist to a goal using motion magic on the TalonSRXs.
   */
  public void runMotionMagic() {
    //this.wristMotor.set(ControlMode.MotionMagic, this.currPosGoal);
    this.wristMotor.set(ControlMode.Position, this.currPosGoal);
  }

  /**
   * Holds the pivot at a specific position using the Position Closed Loop Control.
   */
  public void holdPosition() {
    // Check to see if we're actually at position
    // If we're not at position, don't try to move, just hold the position
    // TODO fix this logic later
    this.wristMotor.set(ControlMode.Position, this.currPosGoal);
  }

  ////////////////////////////////////////////////
  // ALL THE METHODS FOR THE INTAKE/SUCTION CUP //
  ////////////////////////////////////////////////

  /**
   * Runs the suction cup.
   *
   * @param pow The power at which the suction cup sucks.
   */
  public void runSuction(double pow) {
    if (pow <= 0.0) {
      this.suctionMotor.stopMotor();
    } else {
      this.suctionMotor.set(pow);
    }
  }

  /**
   * Sets the air release to true to let go of the game piece.
   */
  public void scoreHatch() {
    this.airRelease.set(true);
    //this.airReleaseB.set(true);
  }

  /**
   * Re-engage the release to be able to suction again. Doesn't actually start suctioning.
   */
  public void engageSuction() {
    this.airRelease.set(false);
    //this.airReleaseB.set(false);
  }

  /**
   * Closes the clamp to hold a cargo. Not used in teh state machine.
   */
  public void closeClamp() {
    //this.pivotClamp.set(true);
  }

  /**
   * Opens the clamp to hold a hatch. Not used in the state machine.
   */
  public void openClamp() {
    //this.pivotClamp.set(false);
  }


  /////////////////////////////////
  // ALL THE SETTERS AND CONFIGS //
  /////////////////////////////////

  /**
   * Used to the set the goal for the motion magic OR position control (HOLDING).
   *
   * @param g The goal, given as a double representing encoder clicks/native CTRE units
   */
  public void setMagicPosGoal(double g) {
    //  if (Math.abs(g - this.currPosGoal) <= this.tolerance * 2) {
    //   this.currState = ControlState.HOLDING;
    //  } else {
    this.currState = ControlState.MAGIC;
    //}
    this.currPosGoal = g;
  }

  public void setIntakeState(IntakeGoal.WristIntakeState s) {
    this.currIntakeState = s;
  }

  /**
   * Sets the state of the wrist.
   *
   * @param s Desired wrist state.
   */
  public void setState(ControlState s) {
    this.currState = s;
  }

  /**
   * This is used for setting a static output for the wrist. Will run the wrist at that power in the
   * runWrist method (the MANUAL/IDLE state). This method should be called by the superstructure.
   *
   * @param pow Takes a power values, sets that to the current output.
   */
  public void setStaticOutput(double pow) {
    this.currOutput = pow;
  }

  /**
   * Configures the velocity and acceleration for motion magic.
   *
   * @param cruiseVelocity     The max velocity of the system (cruise velocity)
   * @param cruiseAcceleration The max acceleration of the system (cruise accel)
   */
  public void configMotionMagic(int cruiseVelocity, int cruiseAcceleration) {
    wristMotor.configMotionCruiseVelocity(cruiseVelocity);
    wristMotor.configMotionAcceleration(cruiseAcceleration);
  }

  /////////////////////
  // ALL THE GETTERS //
  /////////////////////

  /**
   * Use this for determining if we currently have a hatch or a cargo.
   *
   * @return The state of the clamp, which is closed when we have a cargo, open for hatch.
   */
  public boolean hasHatch() {
    return !this.clampState;
  }

  /**
   * Method that calculates the angle of the wrist given the current encoder position.
   *
   * @return The current angle in degrees.
   */
  public double getAngleDegrees() {
    return (Constants.WRIST_ZERO - this.getEncoderClicks()) / Constants.CLICKS_PER_DEG;
  }

  /**
   * Returns the number of encoder clicks for an angle given in degrees. Angle ranges - to +
   *
   * @param angle The angle to be ocnverted, given in degrees.
   * @return The equivalent number of encoder clicks.
   */
  public double angleToClicks(double angle) {
    return Constants.WRIST_ZERO - (angle * Constants.CLICKS_PER_DEG);
  }

  /**
   * Simply returns the current number of encoder clicks.
   *
   * @return The current number of encoder clicks.
   */
  public double getEncoderClicks() {
    return this.wristMotor.getSelectedSensorPosition();
  }


  /**
   * Calculates whether or not the wrist is at its position and ready to move to the next part of
   * the state machine.
   *
   * @return If the wrist is done moving or not.
   */
  public boolean checkTermination() {
    return Math.abs(this.getEncoderClicks() - this.currPosGoal) <= this.tolerance;
  }

  /**
   * Considered done if the wrist is in the HOLDING state.
   *
   * @return If the arm is done with it's predetermined set of movements.
   */
  public boolean isDone() {
    return this.currState == ControlState.HOLDING;
  }

  /**
   * Determines if the wrist has been calibrated. Will only start the state machine if it's in the
   * proper starting position.
   *
   * @return Whether the robot is in the correct spot/calibrated.
   */
  public boolean isCalibrated() {
    return (this.getEncoderClicks() < 0.0 && this.getEncoderClicks() > 0.0);
  }

  /**
   * Used to find the current of the suction cup motor. This can be used to knowing when a game
   * piece has been acquired.
   *
   * @return The current of the suction motor.
   */
  public double getSuctionCurrent() {
    return (suctionMotor.getOutputCurrent());
  }

  /**
   * Prints necessary information to the smartdashboard.
   */
  public void updateSmartDashboard() {
    SmartDashboard.putNumber("Wrist Selected Sensor Position", this.getEncoderClicks());
    SmartDashboard.putNumber("Wrist Feedback Error:",
            Math.abs(this.getEncoderClicks() - this.currPosGoal));
    SmartDashboard.putNumber("Wrist Output:", this.currOutput);
    SmartDashboard.putString("Wrist State:", this.currState.toString());
    SmartDashboard.putNumber("Wrist Angle:", this.getAngleDegrees());
    SmartDashboard.putNumber("Wrist Goal:", this.currPosGoal);
    SmartDashboard.putBoolean("Clamp State", this.clampState);
  }

  @Override
  public void initDefaultCommand() {
  }

}
